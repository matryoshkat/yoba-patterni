using System;
using AbstractFactory.Entities;
using AbstractFactory.Interfaces;
using AbstractFactory.MovieFactories;

namespace AbstractFactory
{
    public class AbstractFactoryController
    {
        public IMovieRentalFactory GetMovieRentalFactory(Language language)
        {
            switch (language)
            {
                case Language.English:
                    return new EnglishLanguageMovieRentalFactory();
                case Language.Russian:
                    return new RussianLanguageMovieRentalFactory();
                case Language.Ukranian:
                    return new UkranianLanguageMovieRentalFactory();
                default:
                    throw new NotImplementedException();
            }
        }
    }
}