using AbstractFactory.Entities;

namespace AbstractFactory.Interfaces
{
    public interface IMovieRentalFactory
    {
        Movie GetMovie(string name);
    }
}