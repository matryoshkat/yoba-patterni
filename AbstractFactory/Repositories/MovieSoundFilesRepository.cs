using System.Collections.Generic;
using System.Linq;
using AbstractFactory.Entities;

namespace AbstractFactory.Repositories
{
    class MovieSoundFilesRepository
    {
        public static List<SoundFile> SoundFiles => new List<SoundFile>{
            new SoundFile{
                Id = 1,
                FilmId = 1,
                Language = Language.English,
                Data = new byte[1]
            },
            new SoundFile{
                Id = 2,
                FilmId = 1,
                Language = Language.Russian,
                Data = new byte[1]
            },
            new SoundFile{
                Id = 3,
                FilmId = 1,
                Language = Language.Ukranian,
                Data = new byte[1]
            },
            new SoundFile{
                Id = 4,
                FilmId = 2,
                Language = Language.English,
                Data = new byte[1]
            },
            new SoundFile{
                Id = 5,
                FilmId = 2,
                Language = Language.Russian,
                Data = new byte[1]
            },
            new SoundFile{
                Id = 6,
                FilmId = 2,
                Language = Language.Ukranian,
                Data = new byte[1]
            }
        };

        public static SoundFile GetByFilmId(int id, Language language) =>
            SoundFiles.Where(s => 
                s.FilmId == id && s.Language == language).FirstOrDefault();
    }
}