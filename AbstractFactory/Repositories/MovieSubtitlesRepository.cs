using System.Collections.Generic;
using System.Linq;
using AbstractFactory.Entities;

namespace AbstractFactory.Repositories
{
    class MovieSubtitlesRepository
    {
        public static List<Subtitle> MoviesSubtitles => new List<Subtitle>{
            new Subtitle{
                Id = 1,
                FilmId = 1,
                Language = Language.English,
                Data = new byte[1]
            },
                new Subtitle{
                Id = 2,
                FilmId = 1,
                Language = Language.Russian,
                Data = new byte[1]
            },
                new Subtitle{
                Id = 3,
                FilmId = 1,
                Language = Language.Ukranian,
                Data = new byte[1]
            },
                new Subtitle{
                Id = 4,
                FilmId = 1,
                Language = Language.English,
                Data = new byte[1]
            },
                new Subtitle{
                Id = 5,
                FilmId = 1,
                Language = Language.Russian,
                Data = new byte[1]
            },
                new Subtitle{
                Id = 6,
                FilmId = 1,
                Language = Language.Ukranian,
                Data = new byte[1]
            },
        };

        public static Subtitle GetByFilmId(int id, Language language) =>
            MoviesSubtitles.Where(s => 
                s.FilmId == id && s.Language == language).FirstOrDefault();
    }
}