﻿using System.Collections.Generic;
using System.Linq;
using AbstractFactory.Entities;

namespace AbstractFactory.Repositories
{
    class MovieBasicDataRepository
    {
        public static List<MovieBasicData> MovieMainData => new List<MovieBasicData>{
            new MovieBasicData{
                Id = 1,
                FilmName = "The Room",
                VideoTrack = new byte[2]
            },
            new MovieBasicData{
                Id = 2,
                FilmName = "Gachimuchi",
                VideoTrack = new byte[69]
            }
        };

        public static MovieBasicData FindByName(string name) =>
            MovieMainData.Where(m => m.FilmName == name).FirstOrDefault();
    }
}