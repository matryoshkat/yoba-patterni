using AbstractFactory.Entities;
using AbstractFactory.Interfaces;

namespace AbstractFactory.MovieFactories
{
    class EnglishLanguageMovieRentalFactory : IMovieRentalFactory
    {
        public Movie GetMovie(string name)
        {
            var movieBasicData = 
                Repositories.MovieBasicDataRepository.FindByName(name);
            
            var englishSubs = 
                Repositories.MovieSubtitlesRepository.GetByFilmId(movieBasicData.Id, Language.English);

            var englishAudio =
                Repositories.MovieSoundFilesRepository.GetByFilmId(movieBasicData.Id, Language.English);
            
            return new Movie(movieBasicData, englishSubs, englishAudio);
        }
    }
}