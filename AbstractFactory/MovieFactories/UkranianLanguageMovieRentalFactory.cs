﻿using AbstractFactory.Entities;
using AbstractFactory.Interfaces;

namespace AbstractFactory.MovieFactories
{
    class UkranianLanguageMovieRentalFactory : IMovieRentalFactory
    {
        public Movie GetMovie(string name)
        {
            var movieBasicData =
                Repositories.MovieBasicDataRepository.FindByName(name);

            var ukrSubs =
                Repositories.MovieSubtitlesRepository.GetByFilmId(movieBasicData.Id, Language.Ukranian);

            var ukrAudio =
                Repositories.MovieSoundFilesRepository.GetByFilmId(movieBasicData.Id, Language.Ukranian);

            return new Movie(movieBasicData, ukrSubs, ukrAudio);
        }
    }
}
