﻿using AbstractFactory.Entities;
using AbstractFactory.Interfaces;

namespace AbstractFactory.MovieFactories
{
    class RussianLanguageMovieRentalFactory : IMovieRentalFactory
    {
        public Movie GetMovie(string name)
        {
            var movieBasicData =
                Repositories.MovieBasicDataRepository.FindByName(name);

            var russianSubs =
                Repositories.MovieSubtitlesRepository.GetByFilmId(movieBasicData.Id, Language.Russian);

            var russianAudio =
                Repositories.MovieSoundFilesRepository.GetByFilmId(movieBasicData.Id, Language.Russian);

            return new Movie(movieBasicData, russianSubs, russianAudio);
        }
    }
}