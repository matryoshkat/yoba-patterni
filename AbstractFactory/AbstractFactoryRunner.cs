using AbstractFactory.Entities;
using AbstractFactory.Interfaces;

namespace AbstractFactory
{
    public class AbstractFactoryRunner
    {
        /*
        Розробити систему Кінопрокат. Користувач може  обрати певну  кінострічку, 
            при  замовленні  кінострічки вказується мова звукової доріжки, 
            який збігається з мовою файлу субтитрів.  
        Система  повинна  поставляти  фільм  з  необхідними характеристиками,  
            причому  при  зміні  мови  звукової  доріжки повинен змінюватися і мову файлу субтитрів і навпаки.
        */
        public void Run()
        {
            var controller = new AbstractFactoryController();

            IMovieRentalFactory movieFactory = null;

            var filmName = "The Room";
            var language = Language.Russian;

            movieFactory = controller.GetMovieRentalFactory(language);

            var movie = movieFactory.GetMovie(filmName);
        }
    }
}