﻿namespace AbstractFactory.Entities
{
    public class MovieBasicData
    {
        public int Id { get; set; }
        public string FilmName { get; set; }
        public byte[] VideoTrack { get; set; }
    }
}