namespace AbstractFactory.Entities
{
    public class SoundFile
    {
        public int Id { get; set; }
        public int FilmId { get; set; }
        public Language Language { get; set; }
        public byte[] Data { get; set; }
    }
}