namespace AbstractFactory.Entities
{
    public class Movie
    {
        public Movie(MovieBasicData data, Subtitle subtitles, SoundFile soundFile)
        {
            _movieMainData = data;
            Subtitles = subtitles;
            SoundFile = soundFile;
        }

        private MovieBasicData _movieMainData;
        public Subtitle Subtitles { get; }
        public SoundFile SoundFile { get; }

        public string GetName() => _movieMainData.FilmName;
        public byte[] GetVideo() => _movieMainData.VideoTrack;
        public int GetMainDataId() => _movieMainData.Id;
    }
}