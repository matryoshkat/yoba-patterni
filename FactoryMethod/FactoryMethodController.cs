using System;
using System.Linq;
using FactoryMethod.Interfaces;

namespace FactoryMethod
{
    public class FactoryMethodController
    {
        private Random _random;
        public IShapeFactory ReturnRandomFactory()
        {
            var type = typeof(IShapeFactory);
            var types = System.AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p) && !p.IsInterface)
                .ToList();
            _random = new Random();
            return (IShapeFactory) Activator.CreateInstance(types[_random.Next(0, types.Count - 1)]);
        }
    }
}