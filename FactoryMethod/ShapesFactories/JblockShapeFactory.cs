using FactoryMethod.Interfaces;
using FactoryMethod.Shapes;

namespace FactoryMethod.ShapesFactories
{
    public class JblockShapeFactory : IShapeFactory
    {
        public IShape Create() => new JblockShape();
    }
}
