using FactoryMethod.Interfaces;
using FactoryMethod.Shapes;

namespace FactoryMethod.ShapesFactories
{
    public class IblockShapeFactory : IShapeFactory
    {
        public IShape Create() => new IblockShape();
    }
}