using FactoryMethod.Interfaces;
using FactoryMethod.Shapes;

namespace FactoryMethod.ShapesFactories
{
    public class TblockShapeFactory : IShapeFactory
    {
        public IShape Create() => new TblockShape();
    }
}