using FactoryMethod.Interfaces;
using FactoryMethod.Shapes;

namespace FactoryMethod.ShapesFactories
{
    public class ZblockShapeFactory : IShapeFactory
    {
        public IShape Create() => new ZblockShape();
    }
}