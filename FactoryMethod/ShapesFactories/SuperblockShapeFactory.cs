using FactoryMethod.Interfaces;
using FactoryMethod.Shapes;

namespace FactoryMethod.ShapesFactories
{
    public class SuperblockShapeFactory : IShapeFactory
    {
        public IShape Create() => new SuperblockShape();
    }
}