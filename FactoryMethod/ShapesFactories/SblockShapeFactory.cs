using FactoryMethod.Interfaces;
using FactoryMethod.Shapes;

namespace FactoryMethod.ShapesFactories
{
    public class SblockShapeFactory : IShapeFactory
    {
        public IShape Create() => new SblockShape();
    }
}