using FactoryMethod.Interfaces;

namespace FactoryMethod.Shapes
{
    public class ZblockShape : IShape
    {
        public string Name { get; set; } = "Z-block";
    }
}