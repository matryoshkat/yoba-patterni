using FactoryMethod.Interfaces;

namespace FactoryMethod.Shapes
{
    public class SblockShape : IShape
    {
        public string Name { get; set; } = "S-block";
    }
}