using FactoryMethod.Interfaces;

namespace FactoryMethod.Shapes
{
    public class LblockShape : IShape
    {
        public string Name { get; set; } = "L-block";
    }
}