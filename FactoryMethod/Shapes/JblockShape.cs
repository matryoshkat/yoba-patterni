using FactoryMethod.Interfaces;

namespace FactoryMethod.Shapes
{
    public class JblockShape: IShape
    {
        public string Name { get; set; } = "J-block";        
    }
}