using FactoryMethod.Interfaces;

namespace FactoryMethod.Shapes
{
    public class TblockShape : IShape
    {
        public string Name { get; set; } = "T-block";
    }
}