using FactoryMethod.Interfaces;

namespace FactoryMethod.Shapes
{
    public class SuperblockShape : IShape
    {
        public string Name { get; set; } = "Super-block";
    }
}