using FactoryMethod.Interfaces;

namespace FactoryMethod.Shapes
{
    public class OblockShape : IShape
    {
        public string Name { get; set; } = "O-block";
    }
}