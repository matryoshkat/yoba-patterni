using FactoryMethod.Interfaces;

namespace FactoryMethod.Shapes
{
    public class IblockShape : IShape
    {
        public string Name { get; set; } = "I-block";
    }
}