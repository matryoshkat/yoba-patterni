using System.Collections.Generic;
using FactoryMethod.Interfaces;

namespace FactoryMethod
{
    public class FactoryMethodRunner
    {
        /*
        Фігури гри «тетріс». 
        Реалізувати процес випадковоговибору  фігури  з фіксованогонабору  фігур. 
        передбачити появусупер-фігур із збільшенимчислом клітин, ніж звичайні.
        */
        public void Run() 
        {
            var controller = new FactoryMethodController();
            var shapesList = new List<IShape>();

            for (int i = 0; i < 10; i++)
            {
                var randomFactory = controller.ReturnRandomFactory();
                shapesList.Add(randomFactory.Create());
            }

        }
    }
}