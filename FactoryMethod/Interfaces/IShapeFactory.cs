namespace FactoryMethod.Interfaces
{
    public interface IShapeFactory
    {
        IShape Create();
    }
}