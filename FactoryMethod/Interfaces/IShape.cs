namespace FactoryMethod.Interfaces
{
    public interface IShape
    {
        string Name { get; set; }
    }
}