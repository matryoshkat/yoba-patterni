using Strategy.Interfaces;
using Strategy.MoveStrategies;

namespace Strategy.Characters
{
    public class Harpy : ICharacter
    {
        public Harpy()
        {
            MovingStrategy = new WalkingStrategy();
        }

        public IMoveStrategy MovingStrategy { get; private set; }

        public void Move() => MovingStrategy.Move();

        public void StartFlying()
        {
            MovingStrategy = new FlyingStrategy();
        }

        public void StartWalking()
        {
            MovingStrategy = new WalkingStrategy();
        }
    }
}