using Strategy.Interfaces;
using Strategy.MoveStrategies;

namespace Strategy.Characters
{
    public class Elf : ICharacter
    {
        public Elf()
        {
            MovingStrategy = new WalkingStrategy();
        }

        public IMoveStrategy MovingStrategy { get; private set; }

        public void Move()
        {
            MovingStrategy.Move();
        }
    }
}