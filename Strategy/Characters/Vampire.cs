using Strategy.Interfaces;
using Strategy.MoveStrategies;

namespace Strategy.Characters
{
    public class Vampire : ICharacter
    {
        public Vampire()
        {
            MovingStrategy = new WalkingStrategy();
        }

        public IMoveStrategy MovingStrategy { get; private set; }

        public void StartFlyingWithMaaagic()
        {
            MovingStrategy = new FlyingStrategy();
        }

        public void StopFlyingWithMaaagic()
        {
            MovingStrategy = new WalkingStrategy();
        }

        public void Move() => MovingStrategy.Move();
    }
}