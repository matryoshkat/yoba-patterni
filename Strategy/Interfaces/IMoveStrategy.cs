namespace Strategy.Interfaces
{
    public interface IMoveStrategy
    {
         void Move();
    }
}