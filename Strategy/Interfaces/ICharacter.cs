namespace Strategy.Interfaces
{
    public interface ICharacter
    {
        IMoveStrategy MovingStrategy { get; }
        void Move();
    }
}