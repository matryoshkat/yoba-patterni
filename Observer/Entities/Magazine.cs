using Observer.Interfaces;

namespace Observer.Entities
{
    public class Magazine : IBook
    {
        public string Name { get; set; }
        public string Content { get; set; }
    }
}