using Observer.Interfaces;

namespace Observer.Entities
{
    public class Newspapper : IBook
    {
        public string Name { get; set; }
        public string Content { get; set; }
    }
}