using Observer.Interfaces;

namespace Observer.Entities
{
    public class BookSubscriber : IObserver
    {
        public string Name { get; set; }

        public void Update(IBook book)
        {
            
        }
    }
}