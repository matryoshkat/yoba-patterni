using System.Collections.Generic;
using Observer.Interfaces;

namespace Observer
{
    public class PostOffice : IObservable
    {
        public List<IObserver> Subscribers { get; set; } = new List<IObserver>();
        public List<IBook> Books { get; set; } = new List<IBook>();

        public void Notify(IBook newBook)
        {
            Subscribers.ForEach(s => s.Update(newBook));
        }

        public void SubscribeClient(IObserver client)
        {
            Subscribers.Add(client);
        }

        public void UnsubscribeClient(IObserver client)
        {
            Subscribers.Remove(client);
        }

        public void AddNewBook(IBook book)
        {
            Books.Add(book);
            Notify(book);
        }

    }
}