﻿namespace Observer.Interfaces
{
    public interface IObservable
    {
        void Notify(IBook book);
    }
}