namespace Observer.Interfaces
{
    public interface IBook
    {
        string Name { get; set; }
        string Content { get; set; }
    }
}