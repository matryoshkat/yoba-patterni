using Observer.Entities;

namespace Observer
{
    public class ObserverRunner
    {
        public void Run()
        {
            var postOffice = new PostOffice();

            var client1 = new BookSubscriber();
            var client2 = new BookSubscriber();

            postOffice.SubscribeClient(client1);
            postOffice.SubscribeClient(client2);

            postOffice.AddNewBook(new Magazine{Name = "asdasd"});

        }
    }
}