using System;
using System.Collections.Generic;
using System.Linq;

namespace Prototype
{
    public class WikiArticlesController
    {
        public WikiArticle FindLastByTitle(string title) =>
            WikiArticlesRepository.WikiArticles
            .Where(a => a.Title == title)
            .OrderByDescending(a => a.CreateDate)
            .Take(1)
            .Select(a => a.Clone())
            .FirstOrDefault();

        public List<WikiArticle> FindAllByTitle(string title) =>
            WikiArticlesRepository.WikiArticles
            .Where(a => a.Title == title)
            .OrderByDescending(a => a.CreateDate)
            .Select(a => a.Clone())
            .ToList();

        public void SaveArticleChanges(WikiArticle article)
        {
            article.CreateDate = DateTime.Now;
            WikiArticlesRepository.WikiArticles.Add(article.Clone());
        }

        public void RevertArticleToGiven(WikiArticle article) =>
            WikiArticlesRepository.WikiArticles
            .RemoveAll(a =>
                a.Title == article.Title &&
                a.CreateDate > article.CreateDate
            );

    }
}