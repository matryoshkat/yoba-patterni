namespace Prototype.Interfaces
{
    public interface IWikiArticlePrototype
    {
         WikiArticle Clone();
    }
}