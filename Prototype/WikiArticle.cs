﻿using System;
using Prototype.Interfaces;

namespace Prototype
{
    public class WikiArticle : IWikiArticlePrototype
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime CreateDate { get; set;} = DateTime.Now;

        public WikiArticle Clone()
        {
            return new WikiArticle{
                Title = Title,
                Text = Text,
                CreateDate = CreateDate
            };
        }
    }
}