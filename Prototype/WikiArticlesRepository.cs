using System.Collections.Generic;

namespace Prototype
{
    public class WikiArticlesRepository
    {
        public static List<WikiArticle> WikiArticles = new List<WikiArticle>
        {
            new WikiArticle
            {
            Title = "Etymology of hippie",
            Text = "According to lexicographer Jesse Sheidlower, the terms hipster and hippie derive from the word hip and the synonym hep, whose origins are unknown.[1] The words hip and hep first surfaced in slang around the beginning of the 20th century and spread quickly, making their first appearance in the Oxford English Dictionary in 1904.",
            },
            new WikiArticle
            {
            Title = "Ludovico Mazzanti",
            Text = "Ludovico Mazzanti (5 December 1686 in Orvieto – 29 August 1775 in Viterbo) was an Italian painter. He was a follower of the school of Giovanni Battista Gaulli, known as Baciccio (died 1709).[1]",
            },
            new WikiArticle
            {
            Title = "Merton (parish)",
            Text = "Merton is an ancient parish which was first in Surrey but since 1965 (as Merton Priory [current parish]) has been in London, bounded by Wimbledon to the north, Mitcham to the east, Morden, Cheam and Cuddington (Worcester Park and rest of Motspur Park) to the south and (New) Malden to the west. The 1871 Ordnance Survey map records its area as 1,764.7 acres (7.1 km2) (2.7 sq mi). "
            }
        };
    }
}