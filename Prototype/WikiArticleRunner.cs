namespace Prototype
{
    public class WikiArticleRunner
    {
        /*
        Існує набір статей в вікіпедії. 
         Реалізувати процес роздачі статей за вимогою для зміни, 
         зберігаючи вихідний варіант для можливого відновлення статті початковому вигляді
        */
        public void Run()
        {
            var controller = new WikiArticlesController();
            var article = controller.FindLastByTitle("Etymology of hippie");
            article.Text = "1st change";

            controller.SaveArticleChanges(article);

            article.Text = "2st change";

            controller.SaveArticleChanges(article);

            var articles = controller.FindAllByTitle(article.Title);

            var firstVersionArticle = articles[articles.Count - 1];

            controller.RevertArticleToGiven(firstVersionArticle);

            articles = controller.FindAllByTitle(article.Title);

        }
    }
}